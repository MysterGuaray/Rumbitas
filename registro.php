<?php     
session_start();

//FACEBOOK SDKV5
require_once __DIR__ . '/vendor/autoload.php';
//NEW FACEBOOK PON TU ID Y APP
$fb = new Facebook\Facebook([
  'app_id' => '1232392006868158',
  'app_secret' => 'a7b68e0c6dd3d83f80eea1e59d4f4a90',
  'default_graph_version' => 'v2.2',
  ]);
$helper = $fb->getRedirectLoginHelper();
$permissions = ['email']; // optional
$loginUrl = $helper->getLoginUrl('http://localhost/Rumbitas/login.php', $permissions);
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Registro</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	

	<link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/registro.css" media="screen,projection">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" href="css/light.css" />
    <link rel="stylesheet" href="css/dark.css" />
      <link rel="stylesheet" href="css/transparent.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>
<body>
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script>
         //inicializo barra de navegación
         $(document).ready(function(){
          $(".button-collapse").sideNav();
         });
         //inicializa select--
      $(document).ready(function() {
    $('select').material_select();
  });
        
</script>

<div class="table-container" style="background:#756895;">
  
       <table width="100%" height="20">
          <tr>
              <td width="35" height="35"><a href="#" class="twitter_transp">Twitter<span></span></a></td>
                <td width="35" height="35"><a href="#" class="linkedin_transp">Linkedin<span></span></a></td>
                <td width="35" height="35"><a href="#" class="dropbox_transp">Dropbox<span></span></a></td>
                <td width="35" height="35"><a href="#" class="pinterest_transp">Pinterest<span></span></a></td>
                <td width="35" height="35"><a href="#" class="blogger_transp">Blogger<span></span></a></td>
                <td width="35" height="35"><a href="#" class="lastfm_transp">LastFM<span></span></a></td>
                <td width="35" height="35"><a href="#" class="facebook_transp">Facebook<span></span></a></td>
                <td width="35" height="35"><a href="#" class="dribbble_transp">Dribbble<span></span></a></td>
                <td width="35" height="35"><a href="#" class="soundcloud_transp">Soundcloud<span></span></a></td>
                <td width="35" height="35"><a href="#" class="googleplus_transp">Google Plus<span></span></a></td>
                <td width="35" height="35"><a href="#" class="skype_transp">Skype<span></span></a></td>
                <td width="35" height="35"><a href="#" class="wordpress_transp">Wordpress<span></span></a></td>
                <td width="35" height="35"><a href="#" class="youtube_transp">Youtube<span></span></a></td>
                <td width="35" height="35"><a href="#" class="yahoo_transp">yahoo<span></span></a></td>
        		<td width="35" height="35"><a href="#" class="digg_transp">Digg<span></span></a></td>
                <td width="35" height="35"><a href="#" class="youtubeclassic_transp">Youtube Classic<span></span></a></td>
                <td width="35" height="35"><a href="#" class="windowslive_transp">Windows Live<span></span></a></td>
                <td width="35" height="35"><a href="#" class="stumbleupon_transp">stumbleupon<span></span></a></td>
            </tr>
        </table>
    
</div>


<nav>
      <div class="nav-wrapper #7986cb indigo lighten-2" id="barranav">
    
      <a href="#!" class="brand-logo yellow-text">RUMBITAS</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
      <li><a href="index.html" class="white-text">Inicio</a></li>
        <li><a href="sass.html" class="white-text">Eventos</a></li>
        <li><a href="badges.html" class="white-text">Descarga App</a></li>
        <li><a href="regasoc.php" class="white-text">Registro Asociados</a></li>
        <li><a href="contacto.html" class="white-text">contacto</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
      <li><a href="index.html" class="red-text">Inicio</a></li>
        <li><a href="sass.html" class="red-text">Eventos</a></li>
        <li><a href="sass.html" class="red-text">Descarga App</a></li>
        <li><a href="regasoc.php" class="red-text">Registro Asociados</a></li>
        <li><a href="contacto.html" class="red-text">contacto</a></li>
        
      </ul>
      </div>
      </nav>


<div class="container">
	<div class="row">
		<div class="col s12 m12 l12">
			<div id="form-login">
				<form action="registro.php" class="col s12" method="post">
					<div class="row">
						<div class="col s12 cyan">
							<h5 class="center login-title">Registrate Gratis</h5>
						</div>
					</div>

					<?php   echo '<a  href="'.$loginUrl.'">'; ?> Registrarse con Facebook</a>
					<div class="row">
						<div class="input-field	col s6">
							<label for="name">Nombre</label>
							<input type="text" name="name" id="name" value="">
						</div>	
						<div class="input-field col s6">
							<label for="lastname">Apellido</label>
							<input type="text" name="lastname" id="lastname" value="">
						</div>						
					</div>
					
					<div class="row">
						<div class="input-field col s6">
							<label for="email">Correo</label>
							<input type="email" name="email" id="email" value="">
						</div>
					
						<div class="input-field col s6">
							<label for="remail">Confirmar correo</label>
							<input type="email" name="remail" id="remail" value="">
						</div>
					</div>
					
					<div class="row">
						<div class="input-field col s6">
							<label for="password">Contraseña</label>
							<input type="password" name="password" id="password" value="">
						</div>
						<div class="input-field col s6">
							<label for="password2">Confirmar contraseña</label>
							<input type="password" name="password2" id="password2" value="">
						</div>
					</div>
					<div class="row">
						<button class="btn waves-effect waves-light" type="submit" name="action">Registrar
    					<i class="material-icons right">send</i>
  						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<footer class="page-footer #7986cb indigo lighten-2">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">contenido Pie de Página</h5>
                <p class="grey-text text-lighten-4">organizar filas y columnas según items</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Enlaces</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">enlace 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">enlace 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">enlace 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">enlace 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2016 Copyright Rumbitas
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>